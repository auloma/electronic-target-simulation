import WebSocket from 'ws';
import express from 'express';
import http from  'http';

const port = 1234;
const app = express();
const serveur = http.createServer(app);
const webSocket = new WebSocket.Server({server:serveur});


/**
 * websocket.on()
 * 
 * defines what to do when a client is connected. here we send every second new localisation
 * computed with randomPosition()
 * 
 */
webSocket.on('connection', (client:WebSocket) => {
    console.log("Target connected to computer !");
    let id = 1;
    
    let interval = setInterval( () => {
        let coordo = {
            id:id,
            x:Math.round((Math.random() * 20 - 10)*10)/10,
            y:Math.round((Math.random() * 20 - 10)*10)/10
        };
        client.send(JSON.stringify(coordo));
        id++;
    }, 1000);

    client.on('close', () => {
        console.log("Connection with computer closed.");
        clearInterval(interval);
    });
});

/**
 * 
 * Opens the communication on port {port}
 * 
 */
serveur.listen(port, () => {
    console.log("Target is waiting for a computer on port " + port + "!");
});