# Electronic target simulation

Shooting sport electronic target simulation.
Front : Angular
Back : NodeJS/Typescript

## Server (The target)
- Open the "target" folder.
- Open a terminal, then compile the .ts file :

```sh
npx tsc
```
Then, launch the server :

```sh
node dist/server
```

## Client (The computeur on which the shooter looks at his session)

- Open the "shooting" folder.
- Open another terminal, then launch the angular server :
```sh
ng serve
```

- Open your favorite browser and watch the localhost page on port 4200 ("https://localhost:4200").