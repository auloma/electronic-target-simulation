import { Impact } from './impact';

export var IMPACTS: Impact[] = [
  { id: 11, x: 1.5, y:8.9, score:10.5 },
  { id: 12, x: 3.5 , y:6.5, score:10.5},
  { id: 13, x: 6.8 , y:7.8, score:10.5},
  { id: 14, x: 8.5 , y:8.9, score:10.5},
  { id: 15, x: 5.2 , y:2.5, score:10.5},
  { id: 16, x: 0.2 , y:2.7, score:10.5},
  { id: 17, x:3.4 , y:5.3, score:10.5},
  { id: 18, x:7.8 , y:0.9, score:10.5},
  { id: 19, x: 9.9, y:9.8 , score:10.5},
  { id: 20, x: -7.2 , y:-7.1, score:10.5},
  { id:21, x:0,y:0 , score:10.5},
  { id: 11, x: 1.5, y:8.9 , score:10.5},
  { id: 12, x: 3.5 , y:6.5, score:10.5},
  { id: 13, x: 6.8 , y:7.8, score:10.5},
  { id: 14, x: 8.5 , y:8.9, score:10.5},
  { id: 15, x: 5.2 , y:2.5, score:10.5},
  { id: 16, x: 0.2 , y:2.7, score:10.5},
  { id: 17, x:3.4 , y:5.3, score:10.5},
  { id: 18, x:7.8 , y:0.9, score:10.5},
  { id: 19, x: 9.9, y:9.8, score:10.5 },
  { id: 20, x: -7.2 , y:-7.1, score:10.5},
  { id:21, x:0,y:0 , score:10.5}
];