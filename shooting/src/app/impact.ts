export interface Impact {
  id:number;
  x:number;
  y:number;
  score:number;
}