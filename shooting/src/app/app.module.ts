import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TargetComponent } from './target/target.component';
import { ImpactsListComponent } from './impacts-list/impacts-list.component';

@NgModule({
  declarations: [
    AppComponent,
    TargetComponent,
    ImpactsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }