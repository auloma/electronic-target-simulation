import { Component, OnInit } from '@angular/core';
import { Impact } from '../impact';
import { ImpactsService } from '../impacts.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-target',
  templateUrl: './target.component.html',
  styleUrls: ['./target.component.css']
})
export class TargetComponent implements OnInit {

  public impacts : Impact[];
  public impactsSubscription:Subscription;
 

  constructor(private impactsService: ImpactsService) {
    this.impacts = [];
    this.impactsSubscription = new  Subscription ();
  }

  /**
   * function getImpacts()
   * 
   * subscription to the value of the impactsService impacts array. Its value is used to update this.impacts. 
   * 
   * @returns {void}
   */
  initImpacts(): void {
    this.impactsSubscription = this.impactsService.getImpactSubject().subscribe(newImpact => this.impacts.push(newImpact));
  }

  destroyImpacts() {
    this.impactsSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.initImpacts();
  }

  ngOnDestroy ():void{
    this.destroyImpacts();
  }
}
