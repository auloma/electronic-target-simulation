import { Injectable } from '@angular/core';
import { Impact } from './impact';
import { WebSocketSubject } from 'rxjs/webSocket';
import { Subject } from 'rxjs';

export interface Coordo {
  id:number;
  x:number;
  y:number;
}

@Injectable({
  providedIn: 'root'
})

export class ImpactsService {

  private ws: WebSocketSubject<Coordo>;
  private impactSubject: Subject<Impact>;
  private port:number = 1234;
  private expected:number = 1;
  
  constructor() {
    this.impactSubject = new Subject();
    this.ws = new WebSocketSubject ('ws://localhost:'+this.port);
    this.ws.subscribe( 
      (message) => {
        let impact:Impact = {
          id:message.id,
          x:message.x,
          y:message.y,
          score:(109 - ( Math.sqrt(message.x*message.x*100 + message.y*message.y*100)))/10
        }
        if(impact.score < 1.0)
          impact.score = 0.0;
        if(message.id !== this.expected) {
          let missingShot:Impact = {
            id:this.expected,
            x:10,
            y:10,
            score :0.0
          }
          this.impactSubject.next(missingShot);
          this.expected ++;
        }
        this.impactSubject.next(impact);
        this.expected ++;
      },
      (err) => console.error(err)
    );
  }

  getImpactSubject ():Subject<Impact> {
    return this.impactSubject;
  }

}