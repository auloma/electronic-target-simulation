import { Component, OnInit } from '@angular/core';
import { Impact } from '../impact';
import { ImpactsService } from '../impacts.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-impacts-list',
  templateUrl: './impacts-list.component.html',
  styleUrls: ['./impacts-list.component.css']
})

export class ImpactsListComponent implements OnInit {

  public impacts :Impact[];
  public score:number;
  public computing:string;
  public impactsSubscription:Subscription;

  constructor(private impactsService: ImpactsService) {
    this.impacts = [];
    this.score = 0;
    this.computing = "decimals";
    this.impactsSubscription = new  Subscription ();
  }


  /**
   * function compute()
   * 
   * compute the total score in this.score according to the computing mode contained in this.computing,
   * using the impacts array.
   * This function is called when the user changes the value of the <select> object in the html part of this componnent.
   * 
   * @returns {void}
   */
  compute() {
    let score:number = 0;
    if(this.computing === "integers") {
      for(var i=0; i<this.impacts.length;i++){
        score += Math.floor(this.impacts[i].score);
      }
      this.score = score;
    } else {
      for(var i = 0; i< this.impacts.length;i++){
        score = (score * 10 + Math.round(this.impacts[i].score * 10))/10;
      }
      this.score = Math.floor(score*10)/10;
    }
  }


  /**
   * function getImpacts()
   * 
   * subscription to the value of the impactsService impacts array. Its value is used to update this.impacts. 
   * 
   * @returns {void}
   */
  initImpacts(): void {
    this.impactsSubscription = this.impactsService.getImpactSubject().subscribe(newImpact => {
      this.impacts.push(newImpact);
      if(this.computing === "integers"){
        this.score += Math.floor(newImpact.score);
      } else {
        this.score = (this.score * 10 + Math.round(newImpact.score * 10))/10;
      }
    });
  }


  /**
   *  Unsubscribes impactsSubscription
   */
  destroyImpacts():void{
    this.impactsSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.initImpacts();
  }

  ngOnDestroy():void{
    this.destroyImpacts();
  }
}